import asyncio
from aiohttp import web
import os


###
#
#  Handle the requests
#
###
async def handle(request):
    return web.Response(body=b"Botorosa 0.1b")


###
#
#  M  A  I  N
#
###
if __name__ == "__main__":

    app = web.Application()
    app.router.add_route('GET', '/', handle)

    loop = asyncio.get_event_loop()
    handler = app.make_handler()

    port = int(os.environ.get('PORT', 5000))
    f = loop.create_server(handler, '0.0.0.0', port)
    srv = loop.run_until_complete(f)
    print('serving on', srv.sockets[0].getsockname())

    try:
        loop.run_forever()

    except KeyboardInterrupt:
        pass

    finally:
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(app.shutdown())
        loop.run_until_complete(handler.finish_connections(60.0))
        loop.run_until_complete(app.cleanup())
        loop.close()
