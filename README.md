The aim of the project is create a Restful webservice to analyze a dataset of images, learn with them and then creates a pattern to be used in image recognition.

It uses the following technologies:

* aiohttp - http://aiohttp.readthedocs.io

* tensorflow - http://tensorflow.org

* rabbitmq - https://www.rabbitmq.com/