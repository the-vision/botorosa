from apscheduler.schedulers.blocking import BlockingScheduler
import urllib3

# job
def ping():
    http = urllib3.PoolManager()
    req = http.request('GET', 'http://botorosa.herokuapp.com')
    req.close()

# scheduler
sched = BlockingScheduler()

# start the scheduler
sched.add_job(ping, 'interval', minutes=10)
sched.start()
